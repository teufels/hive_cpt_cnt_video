<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptCntVideo',
            'Hivevideoshowvideo',
            [
                'Video' => 'showVideo'
            ],
            // non-cacheable actions
            [
                'Video' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hivevideoshowvideo {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_cpt_cnt_video') . 'Resources/Public/Icons/user_plugin_hivevideoshowvideo.svg
                        title = LLL:EXT:hive_cpt_cnt_video/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_video_domain_model_hivevideoshowvideo
                        description = LLL:EXT:hive_cpt_cnt_video/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_video_domain_model_hivevideoshowvideo.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntvideo_hivevideoshowvideo
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
call_user_func(
    function($extKey, $globals)
    {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hivevideoshowvideo >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hiveexteventeventlist {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = Video (list)
                            description = List of videos (files, youtube, vimeo)
                            tt_content_defValues {
                                CType = list
                                list_type = hivecptcntvideo_hivevideoshowvideo
                            }
                        }
                        show := addToList(hivevideoshowvideo)
                    }

                }
            }'
        );

        // Register for hook to show preview of tt_content element of CType="yourextensionkey_newcontentelement" in page module
        $globals['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem'][$extKey] =
            \HIVE\HiveExtEvent\Hooks\PageLayoutView\EventListPreviewRenderer::class;

    }, $_EXTKEY, $GLOBALS
);