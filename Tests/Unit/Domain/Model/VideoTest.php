<?php
namespace HIVE\HiveCptCntVideo\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 * @author Yannick Aister <y.aister@teufels.com>
 */
class VideoTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveCptCntVideo\Domain\Model\Video
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveCptCntVideo\Domain\Model\Video();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getFormatReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getFormat()
        );
    }

    /**
     * @test
     */
    public function setFormatForIntSetsFormat()
    {
        $this->subject->setFormat(12);

        self::assertAttributeEquals(
            12,
            'format',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImageReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getImage()
        );
    }

    /**
     * @test
     */
    public function setImageForFileReferenceSetsImage()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImage($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'image',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLargemp4ReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getLargemp4()
        );
    }

    /**
     * @test
     */
    public function setLargemp4ForFileReferenceSetsLargemp4()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setLargemp4($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'largemp4',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLargewebmReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getLargewebm()
        );
    }

    /**
     * @test
     */
    public function setLargewebmForFileReferenceSetsLargewebm()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setLargewebm($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'largewebm',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLargeogvReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getLargeogv()
        );
    }

    /**
     * @test
     */
    public function setLargeogvForFileReferenceSetsLargeogv()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setLargeogv($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'largeogv',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSmallmp4ReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getSmallmp4()
        );
    }

    /**
     * @test
     */
    public function setSmallmp4ForFileReferenceSetsSmallmp4()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setSmallmp4($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'smallmp4',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSmallwebmReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getSmallwebm()
        );
    }

    /**
     * @test
     */
    public function setSmallwebmForFileReferenceSetsSmallwebm()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setSmallwebm($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'smallwebm',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSmallogvReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getSmallogv()
        );
    }

    /**
     * @test
     */
    public function setSmallogvForFileReferenceSetsSmallogv()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setSmallogv($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'smallogv',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getVideoidReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getVideoid()
        );
    }

    /**
     * @test
     */
    public function setVideoidForStringSetsVideoid()
    {
        $this->subject->setVideoid('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'videoid',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPreloadReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getPreload()
        );
    }

    /**
     * @test
     */
    public function setPreloadForIntSetsPreload()
    {
        $this->subject->setPreload(12);

        self::assertAttributeEquals(
            12,
            'preload',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAutoplayReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getAutoplay()
        );
    }

    /**
     * @test
     */
    public function setAutoplayForBoolSetsAutoplay()
    {
        $this->subject->setAutoplay(true);

        self::assertAttributeEquals(
            true,
            'autoplay',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getControlsReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getControls()
        );
    }

    /**
     * @test
     */
    public function setControlsForBoolSetsControls()
    {
        $this->subject->setControls(true);

        self::assertAttributeEquals(
            true,
            'controls',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMutedReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getMuted()
        );
    }

    /**
     * @test
     */
    public function setMutedForBoolSetsMuted()
    {
        $this->subject->setMuted(true);

        self::assertAttributeEquals(
            true,
            'muted',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRatioReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getRatio()
        );
    }

    /**
     * @test
     */
    public function setRatioForIntSetsRatio()
    {
        $this->subject->setRatio(12);

        self::assertAttributeEquals(
            12,
            'ratio',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPlaybyclickReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getPlaybyclick()
        );
    }

    /**
     * @test
     */
    public function setPlaybyclickForBoolSetsPlaybyclick()
    {
        $this->subject->setPlaybyclick(true);

        self::assertAttributeEquals(
            true,
            'playbyclick',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAutopausebyothervideoReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getAutopausebyothervideo()
        );
    }

    /**
     * @test
     */
    public function setAutopausebyothervideoForBoolSetsAutopausebyothervideo()
    {
        $this->subject->setAutopausebyothervideo(true);

        self::assertAttributeEquals(
            true,
            'autopausebyothervideo',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAutopausebyvisibilityReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getAutopausebyvisibility()
        );
    }

    /**
     * @test
     */
    public function setAutopausebyvisibilityForBoolSetsAutopausebyvisibility()
    {
        $this->subject->setAutopausebyvisibility(true);

        self::assertAttributeEquals(
            true,
            'autopausebyvisibility',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLoopvideoReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getLoopvideo()
        );
    }

    /**
     * @test
     */
    public function setLoopvideoForBoolSetsLoopvideo()
    {
        $this->subject->setLoopvideo(true);

        self::assertAttributeEquals(
            true,
            'loopvideo',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getWidthReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getWidth()
        );
    }

    /**
     * @test
     */
    public function setWidthForIntSetsWidth()
    {
        $this->subject->setWidth(12);

        self::assertAttributeEquals(
            12,
            'width',
            $this->subject
        );
    }
}
