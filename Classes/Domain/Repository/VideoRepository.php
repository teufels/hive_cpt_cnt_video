<?php
namespace HIVE\HiveCptCntVideo\Domain\Repository;

/***
 *
 * This file is part of the "hive_cpt_cnt_video" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *
 ***/

/**
 * The repository for Videos
 */
class VideoRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    //     * @var array
    //    protected $defaultOrderings = [
    //        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    //    ];
    /**
     * findByUidListOrderByListIfNotEmpty
     *
     * @param string String containing uids
     * @return \HIVE\HiveCptCntVideo\Domain\Model\Video
     */
    public function findByUidListOrderByListIfNotEmpty($uidList)
    {
        $uidArray = explode(',', $uidList);
        $result = [];
        foreach ($uidArray as $uid) {
            $result[] = $this->findByUid($uid);
        }
        //return only if not empty
        if ($result[0] != NULL) {
            return $result;
        }
    }

    /**
     * Find by Uid
     *
     * @param int $uid
     * @return Video
     */
    public function findByUid($uid)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->matching($query->equals('uid', $uid));
        return $query->execute()->getFirst();
    }

    public function findRaw()
    {
        $query = $this->createQuery();
        //$query->getQuerySettings()->setReturnRawQueryResult(TRUE);
        $query->statement('SELECT * from tx_hivecptcntvideo_domain_model_video');
        return $query->execute(true);
    }
}
