<?php
namespace HIVE\HiveCptCntVideo\ViewHelpers;

/***
 *
 * This file is part of the "hive_cpt_cnt_video" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;
//use TYPO3\CMS\Core\Utility\GeneralUtility;
//use TYPO3\CMS\Core\Resource\FileReference;
//use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
//use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
//use HIVE\HiveUserfuncs\UserFunc\SettingsUserFunc;
//use TYPO3\CMS\Frontend\Service\TypoLinkCodecService;
//
//
//use TYPO3\CMS\Core\Resource\FileInterface;
//use TYPO3\CMS\Core\Resource\Rendering\RendererRegistry;
//use TYPO3\CMS\Extbase\Domain\Model\AbstractFileFolder;
//use TYPO3\CMS\Extbase\Service\ImageService;

//use TYPO3\CMS\Core\Utility\PathUtility;
//use TYPO3\CMS\Core\Resource;
//use TYPO3\CMS\Core\Resource\File;
//use TYPO3\CMS\Core\Resource\Folder;
//use TYPO3\CMS\Core\Utility\GeneralUtility;
//use TYPO3\CMS\Core\Resource\OnlineMedia\Helpers;


use TYPO3\CMS\Core\Resource\DuplicationBehavior;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\Folder;
use TYPO3\CMS\Core\Resource\Index\FileIndexRepository;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Resource\OnlineMedia\Helpers\AbstractOEmbedHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility;


/**
 * @package HIVE\HiveViewhelpers\ViewHelpers
 */

class CptCntVideoViewHelper extends AbstractTagBasedViewHelper
{
//    /**
//    * Mime types that can be used in the HTML Video tag
//    *
//    * @var array
//    */
//    //protected $possibleMimeTypes = ['video/mp4', 'video/webm', 'video/ogg', 'application/ogg'];
//    protected $possibleMimeTypes = ['video/mp4'];

    /**
     * videoRepository
     *
     * @var \HIVE\HiveCptCntVideo\Domain\Repository\VideoRepository
     * @inject
     */
    protected $videoRepository = NULL;

    /**
     * @param array aVideos
     * @return string
     */
    public function render($aVideos) {

        //var_dump($aVideos);die;

        $aFullHtml = [];
        $aFullHtml[] = '<div class="tx-hive-cpt-cnt-video">';

        //get object values
        $aAllVideos = [];
        foreach ($aVideos as $obj) {
            $aVideo = [];
            $aVideo['title'] = $obj->getTitle();
            $aVideo['format'] = $obj->getFormat();
            if ($obj->getImage() != null) {
                $aVideo['image']['uid'] = $obj->getImage()->getOriginalResource()->getUid();
                $aVideo['image']['url'] = $obj->getImage()->getOriginalResource()->getPublicUrl();
            }
            if ($obj->getLargemp4() != null) {
                $aVideo['videolarge']['mp4'] = $obj->getLargemp4()->getOriginalResource()->getPublicUrl();
            }
            if ($obj->getLargewebm() != null) {
                $aVideo['videolarge']['webm'] = $obj->getLargewebm()->getOriginalResource()->getPublicUrl();
            }
            if ($obj->getLargeogv() != null) {
                $aVideo['videolarge']['ogv'] = $obj->getLargeogv()->getOriginalResource()->getPublicUrl();
            }
            if ($obj->getSmallmp4() != null) {
                $aVideo['videosmall']['mp4'] = $obj->getSmallmp4()->getOriginalResource()->getPublicUrl();
            }
            if ($obj->getSmallwebm() != null) {
                $aVideo['videosmall']['webm'] = $obj->getSmallwebm()->getOriginalResource()->getPublicUrl();
            }
            if ($obj->getSmallogv() != null) {
                $aVideo['videosmall']['ogv'] = $obj->getSmallogv()->getOriginalResource()->getPublicUrl();
            }
            $aVideo['videoid'] = $obj->getVideoid();
            $aVideo['option']['preload'] = $obj->getPreload();
            $aVideo['option']['autoplay'] = $obj->getAutoplay();
            $aVideo['option']['controls'] = $obj->getControls();
            $aVideo['option']['muted'] = $obj->getMuted();
            $aVideo['option']['ratio'] = $obj->getRatio();
            $aVideo['option']['autopausebyothervideo'] = $obj->getAutopausebyothervideo();
            $aVideo['option']['autopausebyvisibility'] = $obj->getAutopausebyvisibility();
            $aVideo['option']['playbyclick'] = $obj->getPlaybyclick();
            $aVideo['option']['loop'] = $obj->getLoopvideo();
            $aVideo['option']['width'] = $obj->getWidth();
            $aAllVideos[] = $aVideo;
        }

        //build html
        foreach ($aAllVideos as $obj) {
            if (!empty($obj)) {
                $html = '';
                switch ($obj['format']) {
                    case 1:
                        $html = $this->getHTML5HTML($obj);
                        break;
                    case 2:
                        $html = $this->getYoutubeHTML($obj);
                        break;
                    case 3:
                        $html = $this->getVimeoHTML($obj);
                        break;
                }
                $aFullHtml[] = $html;
            }
        }

        $aFullHtml[] = '</div>';
        $sFullHtml = implode("",$aFullHtml);

        return $sFullHtml;
    }

    /**
     * @param array aVideo
     * @return html
     */
    function getHTML5HTML($aVideo) {

        $aSources = [];

        if (isset($aVideo['videolarge']['mp4']) || !empty($aVideo['videolarge']['mp4'])) {
            $aSources[] = '<source src="' . $aVideo['videolarge']['mp4'] . '" type="video/mp4" media="(min-device-width: 640px)">';
        }

        if (isset($aVideo['videolarge']['webm']) || !empty($aVideo['videolarge']['webm'])) {
            $aSources[] = '<source src="' . $aVideo['videolarge']['webm'] . '" type="video/webm" media="(min-device-width: 640px)">';
        }

        if (isset($aVideo['videolarge']['ogv']) || !empty($aVideo['videolarge']['ogv'])) {
            $aSources[] = '<source src="' . $aVideo['videolarge']['ogv'] . '" type="video/ogv" media="(min-device-width: 640px)">';
        }

        if (isset($aVideo['videosmall']['mp4']) || !empty($aVideo['videosmall']['mp4'])) {
            $aSources[] = '<source src="' . $aVideo['videosmall']['mp4'] . '" type="video/mp4">';
        }

        if (isset($aVideo['videosmall']['webm']) || !empty($aVideo['videosmall']['webm'])) {
            $aSources[] = '<source src="' . $aVideo['videosmall']['webm'] . '" type="video/webm">';
        }

        if (isset($aVideo['videosmall']['ogv']) || !empty($aVideo['videosmall']['ogv'])) {
            $aSources[] = '<source src="' . $aVideo['videosmall']['ogv'] . '" type="video/ogv">';
        }


        $aAttributes = [];

        if (isset($aVideo['option']['preload']) || !empty($aVideo['option']['preload'])) {
            $aAttributes[] = $aVideo['option']['preload'];
        }

        if (isset($aVideo['option']['autoplay']) || !empty($aVideo['option']['autoplay'])) {
            if($aVideo['option']['autoplay'] == true) {
                $aAttributes[] = 'autoplay';
            }
        }

        if (isset($aVideo['option']['controls']) || !empty($aVideo['option']['controls'])) {
            if($aVideo['option']['controls'] == true) {
                $aAttributes[] = 'controls';
            }
        }

        if (isset($aVideo['option']['muted']) || !empty($aVideo['option']['muted'])) {
            if($aVideo['option']['muted'] == true) {
                $aAttributes[] = 'muted';
            }
        }

        if (isset($aVideo['option']['loop']) || !empty($aVideo['option']['loop'])) {
            if($aVideo['option']['loop'] == true) {
                $aAttributes[] = 'loop';
            }
        }

        $aClasses = [];

        if (isset($aVideo['option']['autopausebyothervideo']) || !empty($aVideo['option']['autopausebyothervideo'])) {
            $aClasses[] = 'autopausebyothervideo';
        }

        if (isset($aVideo['option']['autopausebyvisibility']) || !empty($aVideo['option']['autopausebyvisibility'])) {
            $aClasses[] = 'autopausebyvisibility';
        }

        if (isset($aVideo['option']['ratio']) || !empty($aVideo['option']['ratio'])) {
            switch ($aVideo['option']['ratio']) {
                case 0:
                    $sRatio = 'ratio__tv';
                    break;
                case 1:
                    $sRatio = 'ratio__tv--classic';
                    break;
                case 2:
                    $sRatio = 'ratio__square';
                    break;
                case 3:
                    $sRatio = 'ratio__widescreen--cinema';
                    break;
                case 4:
                    $sRatio = 'ratio__anamorphic';
                    break;
            }
        }

        if (isset($aVideo['option']['width']) || !empty($aVideo['option']['width'])) {
            switch ($aVideo['option']['width']) {
                case 0:
                    $sWidth = 'w100';
                    break;
                case 1:
                    $sWidth = 'w50';
                    break;
                case 2:
                    $sWidth = 'w33';
                    break;
                case 3:
                    $sWidth = 'w25';
                    break;
            }
        }

        $aAttributes[] = 'class="' . implode(" ", $aClasses) . '"';

        if (!isset($aVideo['title']) || !empty($aVideo['title'])) {
            $aAttributes[] = 'title="' . $aVideo['title'] . '"';
        }

        if (!isset($aVideo['option']['playbyclick']) || !empty($aVideo['option']['playbyclick'])) {
            $aAttributes[] = 'onClick="this.paused?this.play():this.pause()"';
        }

        if (!isset($aVideo['image']['url']) || !empty($aVideo['image']['url'])) {
            $aAttributes[] = 'poster="' . $aVideo['image']['url'] . '"';
        }

        $html = '<div class="video--container ' . $sWidth . '"><div class="video--ratio ' . $sRatio . '"><video ' . implode(" ", $aAttributes) . '>' . implode('', $aSources) . '</video></div></div>';

        return $html;

        //todo & scripts
    }

    /**
     * @param array aVideo
     * @return html
     */
    function getYoutubeHTML($aVideo) {
        if (!isset($aVideo['videoid']) || !empty($aVideo['videoid'])) {

            $aAttributes[] = 'width="100%"';
            $aAttributes[] = 'height="0"';

            $aAttributes[] = 'frameborder="0"';

            //todo: add attributes/settings

            if (isset($aVideo['option']['loop']) || !empty($aVideo['option']['loop'])) {
                if($aVideo['option']['loop'] == true) {
                    //use playlisttrick to loop video
                    $sLoop = '&loop=' . $aVideo['option']['loop'] . '&playlist=' . $aVideo['videoid'];
                }
            }

            if (isset($aVideo['option']['autoplay']) || !empty($aVideo['option']['autoplay'])) {
                if($aVideo['option']['autoplay'] == true) {
                    $sAutoplay = '&autoplay=1';
                }
            }

            if (isset($aVideo['option']['muted']) || !empty($aVideo['option']['muted'])) {
                if($aVideo['option']['muted'] == true) {
                    $sMute = '&mute=1';
                }
            }

            if (isset($aVideo['option']['controls']) || !empty($aVideo['option']['controls'])) {
                if($aVideo['option']['controls'] == true) {
                    $sControls= '&controls=1';
                } else {
                    $sControls= '&modestbranding=1&autohide=1&showinfo=0&controls=0';
                }
            }

            // todo handle youtube poster
//            if (isset($aVideo['image']['url']) || !empty($aVideo['image']['url'])) {
//                if($aVideo['image']['url'] != NULL) {
//                    //$sPoster = '<div class="poster" style="background-image: url(' . $aVideo['image']['url'] . ')"></div>';
//                    //$sPoster = '<div class="poster" style="background-image: url(' . $aVideo['image']['url'] . ')"></div>';
//                    $sPoster = '<button class="videoPoster js-videoPoster" style="background-image:url(' . $aVideo['image']['url'] . ');">Play video</button>';
//                }
//            }

            if (isset($aVideo['option']['ratio']) || !empty($aVideo['option']['ratio'])) {
                switch ($aVideo['option']['ratio']) {
                    case 0:
                        $aClasses[] = 'ratio__tv';
                        break;
                    case 1:
                        $aClasses[] = 'ratio__tv--classic';
                        break;
                    case 2:
                        $aClasses[] = 'ratio__square';
                        break;
                    case 3:
                        $aClasses[] = 'ratio__widescreen--cinema';
                        break;
                    case 4:
                        $aClasses[] = 'ratio__anamorphic';
                        break;
                }
            }

            if (isset($aVideo['option']['width']) || !empty($aVideo['option']['width'])) {
                switch ($aVideo['option']['width']) {
                    case 0:
                        $sWidth = 'w100';
                        break;
                    case 1:
                        $sWidth = 'w50';
                        break;
                    case 2:
                        $sWidth = 'w33';
                        break;
                    case 3:
                        $sWidth = 'w25';
                        break;
                }
            }

            $aHtml = [];
            //$aHtml[] = '<div class="video--container video--youtube videoWrapper js-videoWrapper ' . $sWidth . '"><figure><div class="video--wrapper video--youtube__wrapper ' . implode(" ", $aClasses) . '">';
            $aHtml[] = '<div class="video--container video--youtube ' . $sWidth . '"><figure><div class="video--wrapper video--youtube__wrapper ' . implode(" ", $aClasses) . '">';
            $aHtml[] = '<iframe id="video--id__' . $aVideo['videoid'] . '"';
            $aHtml[] = 'src="http://www.youtube.com/embed/' . $aVideo['videoid'] . '?enablejsapi=1' . $sAutoplay . $sMute . $sLoop . $sControls . '" ' . implode(" ", $aAttributes) . '>';
            $aHtml[] = '</iframe></div></figure></div>';

            $html = implode('', $aHtml);

            return $html;

            //todo scripts & poster
        }

        return '';
    }

    /**
     * @param array aVideo
     * @return html
     */
    function getVimeoHTML($aVideo) {
        if (!isset($aVideo['videoid']) || !empty($aVideo['videoid'])) {
            //$sPublicURL = 'https://vimeo.com/' . $aVideo['videoid'];

            $aAttributes[] = 'width="100%"';
            $aAttributes[] = 'height="0"';
            $aAttributes[] = 'webkitallowfullscreen mozallowfullscreen allowfullscreen';
            $aAttributes[] = 'frameborder="0"';

            if (isset($aVideo['image']['url']) || !empty($aVideo['image']['url'])) {
                if($aVideo['image']['url'] != NULL) {
                    $sPoster = '<div class="poster" style="background-image: url(' . $aVideo['image']['url'] . ')"></div>';
                }
            }

            if (isset($aVideo['option']['ratio']) || !empty($aVideo['option']['ratio'])) {
                switch ($aVideo['option']['ratio']) {
                    case 0:
                        $aClasses[] = 'ratio__tv';
                        break;
                    case 1:
                        $aClasses[] = 'ratio__tv--classic';
                        break;
                    case 2:
                        $aClasses[] = 'ratio__square';
                        break;
                    case 3:
                        $aClasses[] = 'ratio__widescreen--cinema';
                        break;
                    case 4:
                        $aClasses[] = 'ratio__anamorphic';
                        break;
                }
            }

            if (isset($aVideo['option']['width']) || !empty($aVideo['option']['width'])) {
                switch ($aVideo['option']['width']) {
                    case 0:
                        $sWidth = 'w100';
                        break;
                    case 1:
                        $sWidth = 'w50';
                        break;
                    case 2:
                        $sWidth = 'w33';
                        break;
                    case 3:
                        $sWidth = 'w25';
                        break;
                }
            }

            $aHtml = [];
            $aHtml[] = '<div class="video--container video--vimeo ' . $sWidth . '"><figure><div class="video--wrapper video--vimeo__wrapper ' . implode(" ", $aClasses) . '">';
            $aHtml[] = '<iframe id="video--id__' . $aVideo['videoid'] . '"';
            $aHtml[] = 'src="https://player.vimeo.com/video/' . $aVideo['videoid'] . '?byline=0&autoplay=' . $aVideo['option']['autoplay'] . '&loop=' . $aVideo['option']['loop'] . '&title=' . $aVideo['title'] . '" ' . implode(" ", $aAttributes) . '>';
            $aHtml[] = '</iframe>' . $sPoster . '</div></figure></div>';

            $html = implode('', $aHtml);

            return $html;

            //todo scripts
        }

        return '';

    }

    /**
     * Get oEmbed data url
     *
     * @param string $mediaId
     * @param string $format
     * @return string
     */
    protected function getOEmbedUrl($mediaId, $format = 'json')
    {
        return sprintf(
            'https://vimeo.com/api/oembed.%s?url=%s',
            urlencode($format),
            urlencode(sprintf('https://vimeo.com/%s', $mediaId))
        );
    }

    public function curl_get($url) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        $return = curl_exec($curl);
        curl_close($curl);
        return $return;
    }

    function accessProtected($obj, $prop) {
        $reflection = new ReflectionClass($obj);
        $property = $reflection->getProperty($prop);
        $property->setAccessible(true);
        return $property->getValue($obj);
    }




}
